import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#009688',
        secondary: '#3f51b5',
        accent: '#e91e63',
        error: '#f44336',
        warning: '#cddc39',
        info: '#2196f3',
        success: '#00bcd4'
      },
      dark: {
        primary: '#34495E',
        anchor: '#41B883'
      }
    }
  }
})
